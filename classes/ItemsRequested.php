<?php namespace Panatau\MintaDataPPID\Classes;

use Panatau\MintaDataPPID\Models\Permintaan;

trait ItemsRequested
{
    /**
     * Kembalikan item permintaan yang telah dibuat oleh pengguna sebelumnya, dengan array assoc
     * key adalah id permintaan dan value adalah kebutuhan yang telah diringkas.
     * @param mixed $email 
     * @param mixed $hp 
     * @param mixed $nomor_identitas 
     * @return array 
     */
    protected function getItemsRequested($email, $hp, $nomor_identitas)
    {
        if(empty($email) && empty($hp) && empty($nomor_identitas)) {
            return [];
        }
        $permintaan = Permintaan::where(function($query) use($email, $hp, $nomor_identitas) {
            $first = true;
            if(!empty($email)) {
                $first = false;
                $query = $query->where('email', $email);
            }
            if(!empty($hp)) {
                if(!$first) {
                    $query = $query->orWhere('hp', $hp);
                } else {
                    $first = false;
                    $query = $query->where('hp', $hp);
                }
            }
            if(!empty($nomor_identitas)) {
                if(!$first) {
                    $query = $query->orWhere('nomor_identitas', $nomor_identitas);
                } else {
                    $query = $query->where('nomor_identitas', $nomor_identitas);
                }
            }
        })->get();
        $options = [];
        foreach ($permintaan as $p) {
            $options[$p->id] = $p->kebutuhan_short . ' ( diajukan pada ' . $p->created_at->format('d/m/Y') . ' )';
        }
        return $options;

    }
}