<?php namespace Panatau\MintaDataPPID\Classes;

class StatusPermintaan
{
    const STATUS_MENUNGGU = 0;
    const STATUS_DITERIMA = 1;
    const STATUS_DITOLAK = 2;
    const STATUS_DIBATALKAN = 3;
    const STATUS_BANDING = 4;

    public static function getStatusPermintaanOptions()
    {
        return [
            self::STATUS_MENUNGGU => 'Status Menunggu',
            self::STATUS_DITERIMA => 'Status Diterima',
            self::STATUS_DITOLAK => 'Status Ditolak',
            self::STATUS_BANDING => 'Status Banding',
        ];
    }

    public static function getStatusPermintaanLabel($status)
    {
        $options = self::getStatusPermintaanOptions();
        return $options[$status];
    }
}