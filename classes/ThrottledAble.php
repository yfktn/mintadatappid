<?php namespace Panatau\MintaDataPPID\Classes;

use Carbon\Carbon;

trait ThrottledAble
{

    /**
     * Apakah posting datanya di batasi? Cakupan dalam menit dari $inMinute maksimal hanya bisa posting
     * sebanyak $maxCount.
     * @param mixed $ip 
     * @return bool 
     */
    protected function isThrottled($model, $ip, $inMinute = 5, $maxCount = 2)
    {
        // in 5 minutes no more than 2
        $now = Carbon::now();
        $min5Minute = $now->copy()->subMinutes($inMinute);
        // dapatkan dari 5 menit sebelum sampai sekarang, apakah ada yang sama ip address nya sama?
        $count = $model::where('ip_address', $ip)->whereBetween('created_at', [$min5Minute, $now])->count();
        return $count > $maxCount;
    }

}