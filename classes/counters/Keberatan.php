<?php namespace Panatau\MintaDataPPID\Classes\Counters;

use Panatau\MintaDataPPID\Models\Keberatan as KeberatanModel;

class Keberatan 
{
    public static function getCounter()
    {
        return \Cache::remember('counter-keberatan-menu', 120, function() {

            return KeberatanModel::where('tanggapan_pada', null)
                ->orWhere('status_tanggapan', null)
                ->count();
        });
    }
}