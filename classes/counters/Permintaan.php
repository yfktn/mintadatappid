<?php namespace Panatau\MintaDataPPID\Classes\Counters;

use Panatau\MintaDataPPID\Models\Permintaan as PermintaanModel;

class Permintaan 
{
    public static function getCounter()
    {

        return \Cache::remember('counter-permintaan-menu', 120, function() {

            return PermintaanModel::where('status_permintaan', 0)
                ->orWhere('status_pemenuhan', 0)
                ->count();
        });
    }
}