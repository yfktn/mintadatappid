<?php namespace Panatau\MintaDataPPID\Components;

use ApplicationException;
use Cms\Classes\ComponentBase;
use Flash;
use Html;
use Panatau\MintaDataPPID\Classes\ItemsRequested;
use Panatau\MintaDataPPID\Classes\ThrottledAble;
use Panatau\MintaDataPPID\Models\Keberatan;
use Request;
use ValidationException;
use Validator;

/**
 * KeberatanComponent Component
 *
 * @link https://docs.octobercms.com/3.x/extend/cms-components.html
 */
class KeberatanComponent extends ComponentBase
{
    public $dataKeberatan = [];
    use ItemsRequested;
    use ThrottledAble;

    public function componentDetails()
    {
        return [
            'name' => 'KeberatanComponent Component',
            'description' => 'Pengajuan Keberatan untuk PPID'
        ];
    }

    /**
     * @link https://docs.octobercms.com/3.x/element/inspector-types.html
     */
    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->dataKeberatan['jenisIdentitas'] = config('panatau.mintadatappid::pilihan_identitas');
        $this->dataKeberatan['alasanKeberatan'] = config('panatau.mintadatappid::alasan_keberatan');
        $this->dataKeberatan['csrf_token'] = csrf_token();
    }

    /**
     * Load data permintaan sebelumnya yang telah dibuat oleh pengguna, di mana penentuannya diambil
     * dari email, nomor hp, atau nomor identitas.
     * Untuk template yang di render telah ditentukan di code sebelumnya.
     * @return void 
     */
    public function onLoadRequestItems() 
    {
        // dapatkan user ip
        $ip = Request::ip();
        if($this->isThrottled(new Keberatan(), $ip, 5, 10)) {
            throw new ApplicationException("Request Throttled! Terlalu banyak posting dari IP yang sama.");
        }

        // semua posting dimasukkan di sini
        $post = post();
        $this->dataKeberatan['itemsRequested'] = $this->getItemsRequested(
            empty($post['email'])? null: $post['email'],
            empty($post['hp'])? null: $post['hp'],
            empty($post['nomor_identitas'])? null:$post['nomor_identitas']
        );

        // trace_log(count($this->dataKeberatan['itemsRequested']));
        // ini sudah ditentukan di script HTML data-nya!
        // return [
        //     '#permintaan_id' => $this->renderPartial('@referensiPermintaan.htm')
        // ];
    }

    public function onSubmitData()
    {
        if(config('panatau.mintadatappid::aktifkan_recaptcha', false)) {

            $captchaFieldName = post('captcha-field-name');
            // Check for spam?
            $recaptchaValidator = Validator::make(post(), [
                    $captchaFieldName => ['required', new \Yfktn\Altcha\Classes\AltchaValidator($captchaFieldName)],
            ]); 
            if( $recaptchaValidator->fails() ) {
                throw new ApplicationException("Recaptcha gagal divalidasikan!");
            }

        }

        // lakukan proses untuk validasi
        $data = post();

        if(empty($data['csrf_token']) || $data['csrf_token'] != csrf_token()) {
            throw new ApplicationException("CSRF Token Tidak Sesuai!");
        }
        
        // dapatkan user ip
        $ip = Request::ip();
        if($this->isThrottled(new Keberatan(), $ip)) {
            throw new ApplicationException("Request Throttled! Terlalu banyak posting dari IP yang sama.");
        }

        foreach($data as $index => $value)
        {
            $data[$index] = Html::clean($value);
        }

        $rules = [
            'email' => 'required|email',
            'nama' => 'required',
            'hp' => 'required',
            'alamat' => 'required',
            'jenis_identitas' => 'required|in:'. implode(",",array_keys(config('panatau.mintadatappid::pilihan_identitas'))),
            'nomor_identitas' => 'required',
            'alasan_keberatan' => 'required|in:'. implode(",",array_keys(config('panatau.mintadatappid::alasan_keberatan'))),
            'kebutuhan' => 'required',
            'permintaan_id' => 'required',
        ];
        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $keberatan = new Keberatan();
        $keberatan->email = $data['email'];
        $keberatan->nama = $data['nama'];
        $keberatan->hp = $data['hp'];
        $keberatan->alamat = $data['alamat'];
        $keberatan->pekerjaan = $data['pekerjaan'];
        $keberatan->jenis_identitas = $data['jenis_identitas'];
        $keberatan->nomor_identitas = $data['nomor_identitas'];
        $keberatan->alasan_keberatan = $data['alasan_keberatan'];
        $keberatan->kebutuhan = $data['kebutuhan'];
        $keberatan->permintaan_id = $data['permintaan_id'];
        $keberatan->ip_address = $ip;

        if($keberatan->save()) {
            Flash::success('Permintaan telah tersimpan dan diteruskan ke pejabat PPID');
        } else {
           Flash::error('Maaf terjadi kesalahan, silahkan coba lagi');
        }
        // Flash::success('Saat ini data tidak tersimpan karena masih dalam fase prototipe!');
    }
}
