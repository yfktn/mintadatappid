<?php namespace Panatau\MintaDataPPID\Components;

use ApplicationException;
use Cms\Classes\ComponentBase;
use Html;
use Log;
use Panatau\MintaDataPPID\Classes\ItemsRequested;
use Panatau\MintaDataPPID\Classes\ThrottledAble;
use Panatau\MintaDataPPID\Models\Ngecek;
use Panatau\MintaDataPPID\Models\Permintaan;
use Request;
use Validator;

/**
 * UserNgecek Component
 *
 * @link https://docs.octobercms.com/3.x/extend/cms-components.html
 */
class UserNgecek extends ComponentBase
{
    use ThrottledAble;
    use ItemsRequested;

    public $dataNgecek = [];

    public function componentDetails()
    {
        return [
            'name' => 'User Ngecek Status Component',
            'description' => 'Fasilitas user melakukan pengecekan status'
        ];
    }

    /**
     * @link https://docs.octobercms.com/3.x/element/inspector-types.html
     */
    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        
        $this->dataNgecek['csrf_token'] = csrf_token();
    }

    public function onNgecek()
    {
        if(config('panatau.mintadatappid::aktifkan_recaptcha', false)) {

            $captchaFieldName = post('captcha-field-name');
            // Check for spam?
            $recaptchaValidator = Validator::make(post(), [
                    $captchaFieldName => ['required', new \Yfktn\Altcha\Classes\AltchaValidator($captchaFieldName)],
            ]); 
            if( $recaptchaValidator->fails() ) {
                throw new ApplicationException("Recaptcha gagal divalidasikan!");
            }

        }

        $data = post();

        if(empty($data['csrf_token']) || $data['csrf_token'] != csrf_token()) {
            throw new ApplicationException("CSRF Token Tidak Sesuai!");
        }
        
        // dapatkan user ip
        $ip = Request::ip();
        if($this->isThrottled(new Ngecek(), $ip)) {
            throw new ApplicationException("Request Throttled! Terlalu banyak posting dari IP yang sama.");
        }

        $data['email'] = Html::clean($data['email']);
        $data['nomor_identitas'] = Html::clean($data['nomor_identitas']);
        $data['hp'] = Html::clean($data['hp']);

        $objS = new Ngecek();
        $objS->ip_address = $ip;
        $objS->email = $data['email'];
        $objS->nomor_identitas = $data['nomor_identitas'];
        $objS->hp = $data['hp'];
        $objS->save();

        if(empty($data['email']) && empty($data['hp']) && empty($data['nomor_identitas']))
        {
            throw new ApplicationException("Empty request detected!");
        }

        $itemsRequested = $this->getItemsRequested(
            empty($data['email'])? null: $data['email'],
            empty($data['hp'])? null: $data['hp'],
            empty($data['nomor_identitas'])? null:$data['nomor_identitas']
        );

        $this->dataNgecek['itemsRequested'] = Permintaan::where(function($query) use($itemsRequested) {
            $query->whereIn('id', array_keys($itemsRequested));
        })->get();
    }

}
