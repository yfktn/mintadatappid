<?php namespace Panatau\Mintadatappid\Components;

use Cache;
use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;

/**
 * ChartPPID Component digunakan untuk menampilkan statistik permintaan data PPID.
 * Menggunakan chartjs, asumsi kita bahwa template sudah ada melakukan loading terhadap
 * library chartjs nya.
 */
class ChartPPID extends ComponentBase
{
    public $dataChartPPID = [];

    public function componentDetails()
    {
        return [
            'name' => 'ChartPPID Component',
            'description' => 'Menampilkan statistik pengajuan PPID'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->dataChartPPID['StatusPermintaan'] = $this->nilaiBerdasarkanStatusPermintaan();
        $this->dataChartPPID['StatusPemenuhan'] = $this->nilaiBerdasarkanStatusPemenuhan();
        $this->dataChartPPID['PemenuhanLewat'] = $this->nilaiBerdasarkanPemenuhanLewat();
        // trace_log($this->dataChartPPID['PemenuhanLewat']);
    }

    protected function nilaiBerdasarkanStatusPermintaan()
    {
        $until = Carbon::now()->addMinutes(20);
        return Cache::remember('chart_ppid_nilai_status_permintaan', $until, function() {
            $data = DB::table('panatau_mintadatappid_permintaan')
                ->selectRaw('status_permintaan, count(*) as jumlah')
                ->groupBy('status_permintaan')
                ->get();
            $dataC = array_values(array_column($data->toArray(), 'status_permintaan'));
            $dataC = array_map(function($k) {
                return ($k == 0? 'Permintaan Tidak Dapat Dipenuhi': 'Permintaan Dapat Dipenuhi');
            }, $dataC);
            $dataV = array_values(array_column($data->toArray(), 'jumlah'));
            return [
                'labels' => $dataC, 
                'data' => $dataV
            ];
        });
    }

    protected function nilaiBerdasarkanStatusPemenuhan()
    {
        $until = Carbon::now()->addMinutes(20);
        return Cache::remember('chart_ppid_nilai_status_pemenuhan', $until, function() {
            $data = DB::table('panatau_mintadatappid_permintaan')
                ->selectRaw('status_pemenuhan, count(*) as jumlah')
                ->groupBy('status_pemenuhan')
                ->get();
            $dataC = array_values(array_column($data->toArray(), 'status_pemenuhan'));
            $dataC = array_map(function($k) {
                return ($k == 0? 'Belum Dapat Dipenuhi': 'Sudah Bisa Dipenuhi');
            }, $dataC);
            $dataV = array_values(array_column($data->toArray(), 'jumlah'));
            return [
                'labels' => $dataC, 
                'data' => $dataV
            ];
        });
    }

    protected function nilaiBerdasarkanPemenuhanLewat()
    {
        $until = Carbon::now()->addMinutes(20);
        return Cache::remember('chart_ppid_nilai_pemenuhan_lewat', $until, function() {
            $data = DB::table('panatau_mintadatappid_permintaan')
                ->selectRaw('pemenuhan_lewat, count(*) as jumlah')
                ->groupBy('pemenuhan_lewat')
                ->get();
            $dataC = array_values(array_column($data->toArray(), 'pemenuhan_lewat'));
            $dataC = array_map(function($k) {
                return $k;
            }, $dataC);
            $dataV = array_values(array_column($data->toArray(), 'jumlah'));
            return [
                'labels' => $dataC, 
                'data' => $dataV
            ];
        });
    }

}
