<?php namespace Panatau\Mintadatappid\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Flash;
use October\Rain\Exception\ApplicationException;
use Validator;
use ValidationException;
use Panatau\MintaDataPPID\Models\Permintaan;
use Request;
use Html;
use Panatau\MintaDataPPID\Classes\ThrottledAble;

class UserMinta extends ComponentBase
{
    use ThrottledAble;

    public $dataUserMinta = [];

    public function componentDetails()
    {
        return [
            'name'        => 'User Minta Data Component',
            'description' => 'Form untuk meminta data dari PPID ...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    
    public function onRun()
    {
        $this->dataUserMinta['jenisIdentitas'] = config('panatau.mintadatappid::pilihan_identitas');
        $this->dataUserMinta['caraMendapatkan'] = config('panatau.mintadatappid::cara_mendapatkan');
        $this->dataUserMinta['pemenuhanLewat'] = config('panatau.mintadatappid::pemenuhan_lewat');
        $this->dataUserMinta['csrf_token'] = csrf_token();

    }

    /**
     * Lakukan pemrosesan terhadap data yang di submit
     */
    public function onSubmitData()
    {
        if(config('panatau.mintadatappid::aktifkan_recaptcha', false)) {

            $captchaFieldName = post('captcha-field-name');
            // Check for spam?
            $recaptchaValidator = Validator::make(post(), [
                    $captchaFieldName => ['required', new \Yfktn\Altcha\Classes\AltchaValidator($captchaFieldName)],
            ]); 
            if( $recaptchaValidator->fails() ) {
                throw new ApplicationException("Recaptcha gagal divalidasikan!");
            }

        }

        // lakukan proses untuk validasi
        $data = post();

        if(empty($data['csrf_token']) || $data['csrf_token'] != csrf_token()) {
            throw new ApplicationException("CSRF Token Tidak Sesuai!");
        }

        // dapatkan user ip
        $ip = Request::ip();
        if($this->isThrottled(new Permintaan(), $ip)) {
            throw new ApplicationException("Request Throttled! Terlalu banyak posting dari IP yang sama.");
        }

        foreach($data as $index => $value)
        {
            $data[$index] = Html::clean($value);
        }

        $rules = [
            'email' => 'required|email',
            'nama' => 'required',
            'hp' => 'required',
            'alamat' => 'required',
            'kebutuhan' => 'required',
            'pemenuhan_lewat' => 'required|in:'. implode(",",array_keys(config('panatau.mintadatappid::pemenuhan_lewat'))),
            'cara_mendapatkan' => 'required|in:'. implode(",",array_keys(config('panatau.mintadatappid::cara_mendapatkan'))),
            'nomor_identitas' => 'required',
            'jenis_identitas' => 'required|in:'. implode(",",array_keys(config('panatau.mintadatappid::pilihan_identitas'))),
        ];
        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $permintaan = new Permintaan;
        $permintaan->email = $data['email'];
        $permintaan->nama = $data['nama'];
        $permintaan->hp = $data['hp'];
        $permintaan->alamat = $data['alamat'];
        $permintaan->kebutuhan = $data['kebutuhan'];
        $permintaan->pemenuhan_lewat = $data['pemenuhan_lewat'];
        // tambahan di versi 1.0.7
        $permintaan->jenis_identitas = $data['jenis_identitas'];
        $permintaan->cara_mendapatkan = $data['cara_mendapatkan'];
        $permintaan->nomor_identitas = $data['nomor_identitas'];
        $permintaan->ip_address = $ip;

        if($permintaan->save()) {
            Flash::success('Permintaan telah tersimpan dan diteruskan ke pejabat PPID');
        } else {
           Flash::error('Maaf terjadi kesalahan, silahkan coba lagi');
        }
        // Flash::success('Saat ini data tidak tersimpan karena masih dalam fase prototipe!');
    }
}
