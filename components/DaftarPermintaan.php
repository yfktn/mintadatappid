<?php namespace Panatau\MintaDataPPID\Components;

use Cms\Classes\ComponentBase;
use Panatau\MintaDataPPID\Models\Permintaan;

class DaftarPermintaan extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'DaftarPermintaan',
            'description' => 'Menampilkan daftar permintaan data PPID'
        ];
    }

    public function onRun()
    {
        $this->page['daftar_permintaan'] = Permintaan::orderBy('updated_at', 'desc')->get();
    }
}