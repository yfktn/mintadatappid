<?php 
/**
 * Semua nilai index di sini memiliki batasan 30 maksimal karakter!
 */
return [
    'pilihan_identitas' => [
        'ktp' => 'KTP',
        'sim' => 'SIM',
        'passport' => 'Passport',
        'kartu_pelajar' => 'Kartu Pelajar',
        'kartu_mahasiswa' => 'Kartu Mahasiswa',
        'surat_ijin_organisasi' => 'Surat Ijin Organisasi'
    ],
    'cara_mendapatkan' => [
        'dengar_baca_lihat_catat' => 'Melihat/Membaca/Mendengarkan/Mencatat',
        'salinan' => 'Mendapatkan salinan informasi (Hardcopy/Softcopy)',
        'lainnya' => 'Lainnya'
    ],
    'pemenuhan_lewat' => [
        'email' => 'Pemenuhan Lewat Email',
        'ke-kantor' => 'Data Diambil Ke Kantor',
        'fax' => 'Data dikirimkan lewat FAX',
        'pos' => 'Data dikirimkan via pos',
        'kurir' => 'Data dikirimkan via kurir',
    ],
    /**
     * tersedia untuk variable ditambahkan adalah: 
     * 
            {id} = id permintaan,
            {Y} = Tahun 4 angka permintaan dibuat,
            {d} = tanggal permintaan dibuat (1-31),
            {m} = bulan permintaan dibuat (1-12),
            {h} = jam permintaan dibuat (0-23),
     */
    'pola_nomor' => "{id}/OPD/PPID-KALTENG/{Y}",
    "alasan_keberatan" => [
        'permohonan_informasi_ditolak' => 'Permohonan Informasi Ditolak',
        'inf_berkala_tdk_disediakan' => 'Informasi Berkala Tidak Disediakan',
        'permintaan_inf_tdk_ditanggapi' => 'Permintaan Informasi Tidak Ditanggapi',
        'permintaan_inf_tdk_ditanggapi_sb_smestinya' => 'Permintaan Informasi Tidak Ditanggapi Sebagaimana Mestinya',
        'permintaan_inf_tdk_dipenuhi' => 'Permintaan Informasi Tidak Dipenuhi',
        'biaya_yg_dikenakan_tdk_wajar' => 'Biaya Yang Dikenakan Tidak Wajar',
        'inf_disediakan_melebih_jangka_waktu_yg_dttkan' => 'Informasi Disediakan Melebihi Jangka Waktu Yang Ditentukan',
    ],
    'aktifkan_recaptcha' => true,
];