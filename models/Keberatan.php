<?php namespace Panatau\MintaDataPPID\Models;

use Backend\Facades\BackendAuth;
use Db;
use Model;
use Panatau\MintaDataPPID\Classes\ItemsRequested;

/**
 * Model
 */
class Keberatan extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use ItemsRequested;


    /**
     * @var string table in the database used by the model.
     */
    public $table = 'panatau_mintadatappid_keberatan';

    /**
     * @var array rules for validation.
     */
    public $rules = [
        'email' => 'required|email',
        'nama' => 'required',
        'hp' => 'required',
        'alamat' => 'required',
        'jenis_identitas' => 'required',
        'nomor_identitas' => 'required',
        'permintaan_id' => 'required',
        'alasan_keberatan' => 'required',
        'kebutuhan' => 'required',
        'permintaan_id' => 'exists:panatau_mintadatappid_permintaan,id'
    ];

    public $belongsTo = [
        'permintaan' => [
            'Panatau\MintaDataPPID\Models\Permintaan',
            'key' => 'permintaan_id',
        ],
    ];

    public function filterFields($fields, $context = null)
    {
        if($context = 'update') {
            $readOnlyOnUpdate = BackendAuth::getUser()->hasAccess('panatau.mintadatappid.manajemen');
            $fields->email->readOnly = $readOnlyOnUpdate;
            $fields->nama->readOnly = $readOnlyOnUpdate;
            $fields->hp->readOnly = $readOnlyOnUpdate;
            $fields->alamat->readOnly = $readOnlyOnUpdate;
            $fields->jenis_identitas->readOnly = $readOnlyOnUpdate;
            $fields->nomor_identitas->readOnly = $readOnlyOnUpdate;
            $fields->pekerjaan->readOnly = $readOnlyOnUpdate;
            $fields->permintaan_id->readOnly = $readOnlyOnUpdate;
            $fields->alasan_keberatan->readOnly = $readOnlyOnUpdate;
            $fields->kebutuhan->readOnly = $readOnlyOnUpdate;
        }
    }


    public function getAlasanKeberatanOptions()
    {
        return config("panatau.mintadatappid::alasan_keberatan");
    }

    public function getLabelAlasanKeberatanAttribute()
    {
        return $this->getAlasanKeberatanOptions()[$this->attributes['alasan_keberatan']];
    }

    public function getJenisIdentitasOptions()
    {
        return config('panatau.mintadatappid::pilihan_identitas');
    }

    public function getLabelJenisIdentitasAttribute()
    {
        return $this->getJenisIdentitasOptions()[$this->attributes['jenis_identitas']];
    }

    public function getPermintaanIdOptions()
    {
        return $this->getItemsRequested($this->email, $this->hp, $this->nomor_identitas);
    }
}
