<?php namespace Panatau\MintaDataPPID\Models;

use Cache;
use Model;
use Panatau\MintaDataPPID\Classes\StatusPermintaan;

/**
 * Model
 */
class Permintaan extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
        'email' => 'required|email',
        'nama' => 'required',
        'hp' => 'required',
        'alamat' => 'required',
        'kebutuhan' => 'required',
        'pemenuhan_lewat' => 'required',
        'cara_mendapatkan' => 'required',
        'nomor_identitas' => 'required',
    ];

    const PEMENUHAN_LEWAT_EMAIL = 'email';
    const PEMENUHAN_LEWAT_KANTOR = 'ke-kantor';
    const PEMENUHAN_LEWAT_FAX = 'fax';


    public $attachMany = [
        'daftarFilePemenuhan' => ['System\Models\File', 'public'=>false]
    ];

    public function filterFields($fields, $context = null)
    {
        $fields->kebutuhan->readOnly = true;
        if($context == 'create') {
            // kalau catat baru berarti ini mencatat dari orang yang datang ke kantor 
            // secara offline
            $fields->email->readOnly = false;
            $fields->nama->readOnly = false;
            $fields->hp->readOnly = false;
            $fields->alamat->readOnly = false;
            $fields->kebutuhan->readOnly = false;
            $fields->cara_mendapatkan->readOnly = false;
            $fields->nomor_identitas->readOnly = false;
            $fields->jenis_identitas->readOnly = false;
        }
    }

    /**
     * @var string The database table used by the model.
     */
    public $table = 'panatau_mintadatappid_permintaan';

    public function getPemenuhanLewatOptions()
    {
        return config('panatau.mintadatappid::pemenuhan_lewat');
    }

    public function getNomorIdentitasAttribute()
    {
        if(!$this->exists) {
            return '';
        }
        return $this->attributes['nomor_identitas'] === null ? '--- OLD VERSION ---': $this->attributes['nomor_identitas'];
    }

    public function getJenisIdentitasOptions()
    {
        return config('panatau.mintadatappid::pilihan_identitas');
    }

    public function getCaraMendapatkanOptions()
    {
        return config('panatau.mintadatappid::cara_mendapatkan');
    }

    public function getKebutuhanShortAttribute()
    {
        return strip_tags(str_limit($this->kebutuhan, 100));
    }

    /**
     * Kembalikan nomor permintaan yang di generate dari config('panatau.mintadatappid::pola_nomor')
     * @return mixed 
     */
    public function getNomorPermintaanAttribute()
    {
        return Cache::remember("nomor-permintaan-{$this->id}", 120, function () {
            
            $config = config('panatau.mintadatappid::pola_nomor');
            list($tahun, $bulan, $hari, $jam) = explode(" ", $this->created_at->format('Y m d H'));
            $available = [
                '{id}' => $this->id,
                '{Y}' => $tahun,
                '{d}' => $hari,
                '{m}' => $bulan,
                '{h}' => $jam,
            ];
            return str_replace(array_keys($available), array_values($available), $config);
        });
    }

    public function getStatusPermintaanOptions()
    {
        return StatusPermintaan::getStatusPermintaanOptions();
    }

    public function getLabelStatusPermintaanAttribute()
    {
        // bug jika status permintaan = null
        return StatusPermintaan::getStatusPermintaanLabel($this->status_permintaan ?? 0);
    }

    public function getLabelShortKebutuhanAttribute()
    {
        return strip_tags(str_limit($this->kebutuhan, 50));
    }

    public function getLabelNamaMaskedAttribute()
    {
        $length = strlen($this->nama);
        $firstNameLength = floor($length / 2);
        return substr($this->nama, 0, $firstNameLength) . str_repeat('*', $length - $firstNameLength);
    }

}