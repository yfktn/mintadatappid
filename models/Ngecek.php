<?php namespace Panatau\MintaDataPPID\Models;

use Model;

/**
 * Model
 */
class Ngecek extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string table in the database used by the model.
     */
    public $table = 'panatau_mintadatappid_ngecek';

    /**
     * @var array rules for validation.
     */
    public $rules = [
    ];

}
