<?php namespace Panatau\MintaDataPPID;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\Panatau\MintaDataPPID\Components\UserMinta' => 'userMinta',
            '\Panatau\MintaDataPPID\Components\KeberatanComponent' => 'keberatanComponent',
            '\Panatau\MintaDataPPID\Components\UserNgecek' => 'userNgecek',
            '\Panatau\MintaDataPPID\Components\ChartPPID' => 'chartPPID',
            '\Panatau\MintaDataPPID\Components\DaftarPermintaan' => 'daftarPermintaan',
        ];
    }

    public function registerSettings()
    {
    }
}
