<?php namespace Panatau\MintaDataPPID\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class KeberatanOke extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'permintaan_data_ppid' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Panatau.MintaDataPPID', 'main-menu-ppid-minta-data', 'side-keberatan2');
    }
}
