<?php namespace Panatau\MintaDataPPID\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration109 extends Migration
{
    public function up()
    {
        Schema::create('panatau_mintadatappid_keberatan', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('email', 50);
            $table->string('nama', 1024);
            $table->string('hp', 20);
            $table->string('alamat', 2024);
            $table->integer('permintaan_id')->unsigned()->index()->nullable();
            $table->string('pekerjaan', 1024);
            $table->string('alasan_keberatan', 50)->index()->nullable();
            $table->text('kebutuhan');
            $table->text('kasus_posisi')->nullable();
            $table->text('status_tanggapan')->nullable();
            $table->timestamp('tanggapan_pada')->nullable();
            $table->string('jenis_identitas', 30)->index()->nullable();
            $table->string('nomor_identitas', 50)->nullable();
            $table->string('ip_address', 40)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('panatau_mintadatappid_keberatan');
    }
}