<?php namespace Panatau\MintaDataPPID\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePanatauMintadatappidPermintaan extends Migration
{
    public function up()
    {
        Schema::create('panatau_mintadatappid_permintaan', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('email', 50);
            $table->string('nama', 1024);
            $table->smallInteger('pemenuhan_lewat')->nullable()->unsigned();
            $table->text('kebutuhan');
            $table->smallInteger('status_pemenuhan');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('panatau_mintadatappid_permintaan');
    }
}
