<?php namespace Panatau\MintaDataPPID\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePanatauMintadatappidPermintaan2 extends Migration
{
    public function up()
    {
        Schema::table('panatau_mintadatappid_permintaan', function($table)
        {
            $table->string('pemenuhan_lewat', 20)->nullable()->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('panatau_mintadatappid_permintaan', function($table)
        {
            // $table->smallInteger('pemenuhan_lewat')->nullable()->unsigned()->default(null)->change();
        });
    }
}
