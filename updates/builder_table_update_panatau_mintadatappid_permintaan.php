<?php namespace Panatau\MintaDataPPID\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePanatauMintadatappidPermintaan extends Migration
{
    public function up()
    {
        Schema::table('panatau_mintadatappid_permintaan', function($table)
        {
            $table->smallInteger('email_tervalidasi')->nullable()->unsigned()->default(0);
            $table->smallInteger('status_pemenuhan')->nullable()->unsigned()->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('panatau_mintadatappid_permintaan', function($table)
        {
            $table->dropColumn('email_tervalidasi');
            $table->smallInteger('status_pemenuhan')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
