<?php namespace Panatau\MintaDataPPID\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePanatauMintadatappidPermintaan3 extends Migration
{
    public function up()
    {
        Schema::table('panatau_mintadatappid_permintaan', function($table)
        {
            $table->smallInteger('status_permintaan')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('panatau_mintadatappid_permintaan', function($table)
        {
            $table->dropColumn('status_permintaan');
        });
    }
}
