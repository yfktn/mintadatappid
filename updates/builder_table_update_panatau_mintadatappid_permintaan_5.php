<?php namespace Panatau\MintaDataPPID\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePanatauMintadatappidPermintaan5 extends Migration
{
    public function up()
    {
        Schema::table('panatau_mintadatappid_permintaan', function($table)
        {
            $table->string('jenis_identitas', 30)->index()->nullable();
            $table->string('nomor_identitas', 50)->nullable();
            $table->string('cara_mendapatkan', 30)->index()->nullable();
            $table->string('ip_address', 40)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('panatau_mintadatappid_permintaan', function($table)
        {
            $table->dropColumn('cara_mendapatkan');
            $table->dropColumn('nomor_identitas');
            $table->dropColumn('jenis_identitas');
            $table->dropColumn('ip_address');
        });
    }
}