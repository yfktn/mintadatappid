<?php namespace Panatau\MintaDataPPID\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePanatauMintadatappidPermintaan4 extends Migration
{
    public function up()
    {
        Schema::table('panatau_mintadatappid_permintaan', function($table)
        {
            $table->string('hp', 20);
            $table->string('alamat', 2024);
        });
    }
    
    public function down()
    {
        Schema::table('panatau_mintadatappid_permintaan', function($table)
        {
            $table->dropColumn('hp');
            $table->dropColumn('alamat');
        });
    }
}
