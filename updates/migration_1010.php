<?php namespace Panatau\MintaDataPPID\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1010 extends Migration
{
    public function up()
    {
        Schema::create('panatau_mintadatappid_ngecek', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('email', 50);
            $table->string('hp', 20);
            $table->string('nomor_identitas', 50)->nullable();
            $table->string('ip_address', 40)->nullable()->index();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('panatau_mintadatappid_ngecek');
    }
}